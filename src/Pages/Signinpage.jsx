import React from 'react';
import '../Styles/Signinpage.css';
import { ImGoogle } from "react-icons/im";
import { FaFacebookF } from "react-icons/fa6";


const Signinpage = () => {
    return (
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content theme-bg-color">
                    <div class="modal-header flex-column border-0">
                        <p class="modal-title mx-auto text-white" id="exampleModalLabel">Sign In</p>
                        <a href="#" class="signinText py-2 text-white mb-0">Do you have an Flicknexs account?</a>
                    </div>
                    <div class="modal-body">
                        <form class="">
                            <div class="input-box">
                                <input type="email" class="text-white" id="exampleDropdownFormEmail1" placeholder="" required />
                                <label for="exampleDropdownFormEmail1" >Email Address</label>
                            </div>
                            <div class="input-box">
                                <input type="password" class="pt-5 text-white" id="exampleDropdownFormPassword1" placeholder="" required />
                                <label for="exampleDropdownFormPassword1" class="mt-3">Password</label>
                            </div>
                        </form>
                        <button class="signinbutton mb-0 mx-auto d-block text-uppercase px-5 py-1 theme-text-color " type="submit" fdprocessedid="q5c8ss">Sign In</button>
                        <div class="text-center">
                            <a class="dropdown-item text-white pt-2" href="#">Forgot password?</a>
                        </div>
                        <div class="line">
                        </div>
                        <div class="text-center">
                            <a class="Signintext1">Login with using:</a>
                        </div>
                        <div class="socialicon d-flex justify-content-center gap-3 ">
                            <a href="#" class="text-white nav-link"><ImGoogle /></a>
                            <a href="#" class="text-white nav-link socialiconfont"><FaFacebookF /></a>
                        </div>
                        <a class="dropdown-item text-center text-white mt-3 mb-3" href="#">Don't have an account? Sign Up?</a>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Signinpage