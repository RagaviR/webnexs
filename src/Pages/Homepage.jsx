import React from 'react'
import '../Styles/Homepage.css';
import HomeBanner from '../Components/HomePageComponent/HomeBanner';
import HomeText from '../Components/HomePageComponent/HomeText';
import HomeImage from '../Components/HomePageComponent/HomeImage';
import HomeFrontimg from '../Components/HomePageComponent/HomeFrontimg';
import { HomeFrontimgdata, Homefrontimages, Homepic } from '../Components/JSON/JsonData';
import Homepicture from '../Components/HomePageComponent/Homepicture';
import HomeSignup from '../Components/HomePageComponent/HomeSignup';



const Homepage = () => {
  return (
    <div>
      <HomeBanner/>
      <HomeText/>
      <HomeImage/>
      <HomeFrontimg data={HomeFrontimgdata}/>
      <HomeFrontimg data={Homefrontimages}/>
      <Homepicture/>
      <HomeFrontimg data={Homepic}/>
      <HomeSignup/>

    </div>
    
  )
}
export default Homepage