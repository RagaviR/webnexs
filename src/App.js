import logo from './logo.svg';
import './App.css';
import AllRoutes from './Routes/AllRoutes';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js'; 

function App() {
  return (
  
     <AllRoutes/>

  );
}

export default App;
