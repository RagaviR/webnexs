// External Libraries and Packages
import { Route, Routes } from "react-router-dom";
import React, { Suspense } from "react";
import Header from "../Components/Header";

import Footer from "../Components/Footer";
import Homepage from "../Pages/Homepage";
import Layoutpage from "../Components/Layout/Layoutpage";
import NoPage from "../Components/NoPage";


// Internal Styles
// Internal Components



function AllRoutes(props) {
    return (
        <div>
            <Suspense >
                <Routes>
                    <Route path="/" element={<Layoutpage />}>
                        <Route index element={<Homepage />} />
                        <Route path="*" element={<NoPage />} />
                    </Route>
                   
                </Routes>
            </Suspense>
        </div>
    );
}

export default AllRoutes;
