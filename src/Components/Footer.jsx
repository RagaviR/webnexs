import React from 'react'
import '../Styles/Footer.css';
import { TiSocialFacebook } from "react-icons/ti";
import { TiSocialLinkedin } from "react-icons/ti";
import { TiSocialTwitter } from "react-icons/ti";
import { TiSocialInstagram } from "react-icons/ti";
import Logo2 from "../Images/Group.png";

const Footer = () => {
  return (
    <section class="footer">
      <ul class="list">
        <li>
          <a class="nav-link" href="#">Terms Of Use</a>
        </li>
        <li>
          <a class="nav-link" href="#">Privacy Policy</a>
        </li>
        <li>
          <a class="nav-link" href="#">FAQs</a>
        </li>
        <li>
          <a class="nav-link" href="#">Contact Us</a>
        </li>
        <li>
          <a class="nav-link" href="#">Advertise with Us</a>
        </li>
      </ul>
      <div class="social">
        <a href="#" class=" text-white nav-link"><TiSocialFacebook /></a>
        <a href="#" class="text-white nav-link"><TiSocialTwitter /></a>
        <a href="#" class="text-white nav-link"><TiSocialInstagram /></a>
        <a href="#" class="text-white nav-link"><TiSocialLinkedin /></a>
      </div>
      <div class="copyrighttext">
        2021 flicknexs Networks India Pvt. Ltd.
        <div class="copyright">
          <img src={Logo2} alt="" />
        </div>
      </div>
    </section>
  )
}
export default Footer