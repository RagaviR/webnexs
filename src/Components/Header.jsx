import React, { useState } from 'react';
import Logo from "../Images/Flicknexslogo.png";
import '../Styles/Header.css';
import Signinpage from '../Pages/Signinpage';


const Header = () => {
  return (
    <>
    <Signinpage/>
   
    <nav class="navbar navbar-expand-lg navbar-light  fixed-top shadow navbagcol">
      <div class="container-fluid">
        <a class="navbar-brand" href="#"> <img src={Logo} alt="header-logo" /></a>
        <button class="navbar-toggler custom-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#"></a>
            </li>
          </ul>
          <form class="d-flex">
            <button class="btn btn-outline-none text-white" type="button"  data-bs-toggle="modal" data-bs-target="#exampleModal">Sign in</button>
            <button class="button" type="submit">Subscribe Now</button>
          </form>
        </div>
      </div>
    </nav>
    </>
  )
}
export default Header