import React from 'react'
import '../../Styles/Homepage.css'
import { HomeFrontimgdata } from "../JSON/JsonData"

const HomeFrontimg = ({data}) => { 
    return (
        <div class="d-flex overflow-hidden">
            {data?.map((item, index) => {
                return (
                    <div class="pe-1 pb-1">
                        <img src={item.Homeimgfront} />
                    </div>
                );
            })}


        </div>

    )
}

export default HomeFrontimg