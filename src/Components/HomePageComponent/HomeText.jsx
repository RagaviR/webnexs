import React from 'react'
import '../../Styles/Homepage.css';

const HomeText = () => {
  return (
    <div className='homecontent p-5'>
        <h1 className='text-white text-center'>Get 6 months flicknexs Premium Subscription</h1>
        <p className='text-white text-center'>Get exclusive access to latest International shows, all live sports & tv channels etc. with no ads served except in live streaming channels.</p>
    </div>
  )
}

export default HomeText