import React from 'react'
import '../../Styles/Homepage.css'
import pic14 from '../../Images/Icon_3.png'
import pic15 from '../../Images/Icon_2.png'
import pic16 from '../../Images/Icon_1.png'

const HomeSignup = () => {
  return (
    <>
      <div className='d-flex align-items-center justify-content-center flex-column p-5'>
        <h1 className='text-white text-center text-bold pt-2'>All on Your Favorite Devices</h1>
        <h5 className='text-white text-center text-bold pb-5'>Subscription plans vary by subscription provider.</h5>
        <div class="row">
          <div class="col-sm">
            <img class="" src={pic14} alt="" />
            <p className='text-white text-center'>Tv</p>
          </div>
          <div class="col-sm ">
            <img class="" src={pic15} alt="" />
            <p className='text-white text-center'>Computer</p>
          </div>
          <div class="col-sm">
            <img class="" src={pic16} alt="" />
            <p className='text-white text-center'>Mobile & Tablet</p>
          </div>
        </div>
        <div className=''>
          <button type='button' className='rounded-pill bg-transparent text-white border-1 border-white text-bold' >SIGN UP NOW</button>
        </div>
      </div>
    </>

  )
}

export default HomeSignup