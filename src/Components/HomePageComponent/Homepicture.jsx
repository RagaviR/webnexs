import React from 'react'
import BackImage from '../../Images/bg.png'
import '../../Styles/Homepage.css'
import pic14 from '../../Images/bg1.png'

const Homepicture = () => {
    const myStyle = {
        backgroundImage:
            `url(${BackImage})`,
        height: "500px",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center"
    };
    return (
        <>
        <div class="frontimg">
             <img class="position-absolute bottom-10 start-50 homepixcel pt-5" src={pic14} alt=""/>   
        </div>
         <div style={myStyle} className='d-flex  flex-column pt-5'>
            <p className='text-white text-bold pt-5 ps-5 fs-3'>Six seasons of this superficial<br/> melodrama that cuts deep.</p>
            <p className='text-light ps-5 text-bold'>Watch all episodes now.</p>
            <div class="ps-5 ps-5 d-flex justify-content-start pt-3">
                <a class="cus_plyBtn" href="#"></a>
                <button class="plybtn text-white ms-3 mt-1 " href="#">More Info</button>
            </div>
        </div> 
        </>

    )
}

export default Homepicture