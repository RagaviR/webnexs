import React from 'react'
import homeBannerImage from '../../Images/HomeBanner.png'
import '../../Styles/Homepage.css'

const HomeBanner = () => {
    const myStyle = {
        backgroundImage:
            `url(${homeBannerImage})`,
        height: "100vh",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center"
    };
    return (
        <div style={myStyle} className='d-flex align-items-center justify-content-center flex-column  p-2'>
            <h1 className='text-white text-center w-50 bannertext py-5'>Exclusive Originals Movies & International Series</h1>
            <div className='py-5'>
            <p className='text-light text-center'>12 Months</p>
            <button type='button' className='px-3 py-2 Button'>Subscribe Now</button>
            </div>
        </div>
    )
}

export default HomeBanner