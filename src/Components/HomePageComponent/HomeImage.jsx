import React from 'react'
import homeBgimage from '../../Images/n0desjo52.png'
import '../../Styles/Homepage.css'

const HomeImage = () => {
    const myStyle = {
        backgroundImage:
            `url(${homeBgimage})`,
        height: "100vh",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center"
    };
    return (
        <div style={myStyle} className='d-flex align-items-end justify-content-end flex-column  pb-5 pe-5'>
            <h3 className='text-end text-white'>Now Stream on Your Vod</h3>
            <button className='rounded-pill bg-transparent text-white border-1 border-white text-bold'>Watch the Trailer</button>
           
        </div>
    )
}

export default HomeImage